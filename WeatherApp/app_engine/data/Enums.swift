//
//  Enums.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 02/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation


enum ErrorTypes {
    static let invalidJson = "Something went wrong! Please retry."
    static let networkError = "Network error! Please retry."
}

enum StorageKeys {
    static let favedCitiesKey = "favedCitiesKey"
    static let unit = "unitKey"
}
