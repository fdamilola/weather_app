//
//  Data.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 01/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation
import MapKit

struct DataTypes{
    
    typealias JsonObject = [String: AnyObject]
    typealias JsonArray = [AnyObject]
    
    struct FavedCityData{
        var cityName: String = ""
        var countryName: String = ""
        var latitude: Double = 0.0
        var longitude: Double = 0.0
        var cityDescription: String = ""
        
        init() {
            
        }
        
//        init(json: String) {
//            let data = Serializer.toJsonObject(jsonString: json)
//            self.cityName = data["cityName"] as! String
//            self.countryName = data["countryName"] as! String
//            self.latitude = data["latitude"] as! Double
//            self.longitude = data["longitude"] as! Double
//            self.cityDescription = data["cityDescription"] as! String
//        }
        
        init(data: DataTypes.JsonObject) {
            self.cityName = data["cityName"] as! String
            self.countryName = data["countryName"] as! String
            self.latitude = data["latitude"] as! Double
            self.longitude = data["longitude"] as! Double
            self.cityDescription = data["cityDescription"] as! String
        }
        
        var name: String {
            return "\(cityName), \(countryName)"
        }
        
        var json: JsonObject {
            var innerData: JsonObject = [:]
            innerData["cityName"] = cityName as AnyObject
            innerData["countryName"] = countryName as AnyObject
            innerData["latitude"] = latitude as AnyObject
            innerData["longitude"] = longitude as AnyObject
            innerData["cityDescription"] = cityDescription as AnyObject
            return innerData
        }
        
        var jsonString: String? {
            return Serializer.toJsonString(json: json)
        }
        
        var geoCoordinate: CLLocationCoordinate2D {
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        
        static func jsonArray(array : [FavedCityData]) -> String {
            return "[" + array.map {$0.jsonString!}.joined(separator: ",") + "]"
        }
    
    }
    
    //temperature, humidity, rain chances and wind information
    
    struct Weather {
        var temperature: Int = 0
        var humidity: Int = 0
        var rainChances: Int = 0
        var windSpeed: Double = 0.0
        var windDegree: Double =  0.0
        var countryName: String = ""
        var cityName: String = ""
        var icon: String = ""
        var date: Double = 0
        var weatherDescription: String = ""
        var mainWeatherDescription: String = ""
        
        var weatherDescriptionTitleCased: String {
            return weatherDescription.capitalized
        }
        
        
        static func fromJsonObject(data: DataTypes.JsonObject) -> Weather {
            var weather = Weather()
            
            weather.cityName = DataTypes.cleanString(string: data["name"])
            weather.countryName = DataTypes.cleanString(string: DataTypes.cleanJsonObject(json: data["sys"])["country"])
            weather.weatherDescription = DataTypes.cleanString(string: DataTypes.cleanJsonArray(array: data["weather"])[0]["description"])
            weather.mainWeatherDescription = DataTypes.cleanString(string: DataTypes.cleanJsonArray(array: data["weather"])[0]["main"])
            weather.icon = DataTypes.cleanString(string: DataTypes.cleanJsonArray(array: data["weather"])[0]["icon"])
            weather.humidity = DataTypes.cleanInt(integer: DataTypes.cleanJsonObject(json: data["main"])["humidity"])
            weather.temperature = DataTypes.cleanInt(integer: DataTypes.cleanJsonObject(json: data["main"])["temp"])
            weather.windSpeed = DataTypes.cleanDouble(double: DataTypes.cleanJsonObject(json: data["wind"])["speed"])
            weather.date = DataTypes.cleanDouble(double: data["dt"])
            
            //This is a gamble. The api doesn't provide a direct value for chances of rain so I am improvising using the % Cloudiness
            weather.rainChances = DataTypes.cleanInt(integer: DataTypes.cleanJsonObject(json: data["clouds"])["all"])
            return weather
        }
        
        
        
    }
    
    struct Forecast {
        
        var temperature: Int = 0
        var humidity: Int = 0
        var rainChances: Int = 0
        var windSpeed: Double = 0.0
        var windDegree: Double =  0.0
        var countryName: String = ""
        var cityName: String = ""
        var icon: String = ""
        var date: Double = 0
        var weatherDescription: String = ""
        var mainWeatherDescription: String = ""
        
        var dayShort: String {
            return Date(timeIntervalSince1970: date).toDayOfWeek().substring(to: 3)
        }
        
        static func fromJsonObject(data: DataTypes.JsonObject) -> Forecast {
            var forecast = Forecast()
            forecast.weatherDescription = DataTypes.cleanString(string: DataTypes.cleanJsonArray(array: data["weather"])[0]["description"])
            forecast.mainWeatherDescription = DataTypes.cleanString(string: DataTypes.cleanJsonArray(array: data["weather"])[0]["main"])
            forecast.icon = DataTypes.cleanString(string: DataTypes.cleanJsonArray(array: data["weather"])[0]["icon"])
            forecast.humidity = DataTypes.cleanInt(integer: DataTypes.cleanJsonObject(json: data["main"])["humidity"])
            forecast.temperature = DataTypes.cleanInt(integer: DataTypes.cleanJsonObject(json: data["main"])["temp"])
            forecast.windSpeed = DataTypes.cleanDouble(double: DataTypes.cleanJsonObject(json: data["wind"])["speed"])
            forecast.date = DataTypes.cleanDouble(double: data["dt"])
            
            //This is a gamble. The api doesn't provide a direct value for chances of rain so I am improvising using the % Cloudiness
            forecast.rainChances = DataTypes.cleanInt(integer: DataTypes.cleanJsonObject(json: data["clouds"])["all"])
            return forecast
        }
        
        static func fullForecastFromJsonArray(array: [DataTypes.JsonObject]) -> [Forecast] {
            var forecastArray = [Forecast]()
            for item in array {
                let forecast = DataTypes.Forecast.fromJsonObject(data: item)
                forecastArray.append(forecast)
            }
            return forecastArray
        }
        
        static func daysFromJsonArray(array: [DataTypes.JsonObject]) -> [Forecast] {
            var forecastArray = [Forecast]()
            var differenceInDays = 0
            let today = Date()
            for item in array {
                let forecast = DataTypes.Forecast.fromJsonObject(data: item)
                let forecastDate = Date(timeIntervalSince1970: forecast.date)
                if(today.daysBetween(date: forecastDate) == differenceInDays){
                    forecastArray.append(forecast)
                    differenceInDays += 1
                    if(forecastArray.count == 5){
                        //Gotten next 5 days
                        return forecastArray
                    }
                }
            }
            return forecastArray
        }
        
        static func forcastForDate(today: Date, array: [DataTypes.Forecast]) -> [Forecast] {
            var forecastArray = [Forecast]()
            let differenceInDays = 0
            for forecast in array {
                let forecastDate = Date(timeIntervalSince1970: forecast.date)
                if(today.daysBetween(date: forecastDate) == differenceInDays){
                    forecastArray.append(forecast)
                }
            }
            return forecastArray
        }
    }
    
    
    private static func cleanInt(integer: AnyObject?) -> Int {
        return integer == nil ? 0: integer as! Int
    }
    
    private static func cleanDouble(double: AnyObject?) ->  Double {
        return double == nil ? 0: double as! Double
    }
    
    private static func cleanString(string: AnyObject?) -> String {
        return string == nil ? "": string as! String
    }
    
    private static func cleanJsonObject(json: AnyObject?) -> JsonObject {
        return json == nil ? [:]: json as! JsonObject
    }
    
    private static func cleanJsonArray(array: AnyObject?) -> [JsonObject] {
        return array == nil ? []: array as! [JsonObject]
    }
    
}

