//
//  Struvts.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 02/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation

struct Units {
    static let standard = ""
    static let imperial = "imperial"
    static let metric = "metric"
    
    static func getCurrentWindUnit() -> String {
        //Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour
        let currentUnit = LocalStorage.initialize().getCurrentUnitSystem()
        return currentUnit == Units.imperial ? "Mph": currentUnit == Units.metric ? "M/s":"M/s"
    }
    
    static func getCurrentTempUnit() -> String {
        //Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
        let currentUnit = LocalStorage.initialize().getCurrentUnitSystem()
        return currentUnit == Units.imperial ? "°F": currentUnit == Units.metric ? "°C":"°K"
    }
}
