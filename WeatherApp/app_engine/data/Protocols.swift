//
//  Protocols.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 02/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation

protocol CityProtocol {
    func onSaved(savedCity city: DataTypes.FavedCityData)
}
