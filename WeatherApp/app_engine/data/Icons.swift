//
//  Icons.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 02/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

enum Icons {
    
    static let clearSkyDay = "01d"
    static let clearSkyNight = "01n"
    
    static let fewCloudsDay = "02d"
    static let fewCloudsNight = "02n"
    
    static let scatteredCloudsDay = "03d"
    static let scatteredCloudsNight = "03n"
    
    static let brokenCloudsDay = "04d"
    static let brokenCloudsNight = "04n"
    
    static let showerRainDay = "09d"
    static let showerRainNight = "09n"
    
    static let rainDay = "10d"
    static let rainNight = "10n"
    
    static let thunderStormDay = "11d"
    static let thunderStormNight = "11n"
    
    static let snowDay = "13d"
    static let snowNight = "13n"
    
    static let mistDay = "50d"
    static let mistNight = "50n"
    
}

struct IconLoader {
   static func loadIcon(iconId: String) -> UIImage? {
        switch iconId {
        case Icons.clearSkyDay:
            return UIImage(named: "clearSky")
        case Icons.clearSkyNight:
            return UIImage(named: "clearSkyNight")
        case Icons.fewCloudsDay, Icons.scatteredCloudsDay, Icons.brokenCloudsDay:
            return UIImage(named: "fewClouds")
        case Icons.fewCloudsNight, Icons.scatteredCloudsNight, Icons.brokenCloudsNight:
            return UIImage(named: "fewCloudsNight")
        case Icons.showerRainDay, Icons.rainDay:
            return UIImage(named: "rain")
        case Icons.showerRainNight, Icons.rainNight:
            return UIImage(named: "rainNight")
        case Icons.thunderStormDay, Icons.thunderStormNight:
            return UIImage(named: "thunderstorms")
        case Icons.snowDay, Icons.snowNight:
            return UIImage(named: "snow")
        case Icons.mistDay, Icons.mistNight:
            return UIImage(named: "cloudy")
        default:
            return nil
        }
    }
}
