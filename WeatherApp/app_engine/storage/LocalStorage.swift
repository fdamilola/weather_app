//
//  LocalStorage.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 30/06/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation

class LocalStorage {
    
    static func initialize() -> LocalStorage {
        return LocalStorage()
    }
    
    
    /*
     Operations for cities
    */
    
    func saveCity(city: DataTypes.FavedCityData, sprotocol: CityProtocol){
        DispatchQueue.global().async {
            //Note :- a much more better implementation would be to use a Set
            self.saveCity(city: city)
            DispatchQueue.main.async {
                sprotocol.onSaved(savedCity: city)
            }
        }
    }
    
    
    func saveCity(city: DataTypes.FavedCityData){
        //1. Remove city if it exists
        var savedCities = self.removeCity(city: city, cities: self.getSavedCities())
        //2. Add City
        savedCities.append(city)
        //3. Persist cities
        self.persistString(withKey: StorageKeys.favedCitiesKey, withValue: DataTypes.FavedCityData.jsonArray(array: savedCities))
    }
    
    func getSavedCities() -> [DataTypes.FavedCityData] {
        var arrayCities: [DataTypes.FavedCityData] = []

        guard let arrayString = self.getString(withKey: StorageKeys.favedCitiesKey) else {
            return arrayCities
        }
        
        arrayCities = Serializer.toJsonArray(jsonArrayString: arrayString).map({ DataTypes.FavedCityData(data: $0 as! DataTypes.JsonObject) })
        
        return arrayCities
    }
    
    func getReversedSavedCities() -> [DataTypes.FavedCityData] {
        return self.getSavedCities().reversed()
    }
    
    func removeCity(city: DataTypes.FavedCityData, cities: [DataTypes.FavedCityData] = []) -> [DataTypes.FavedCityData] {
        var cities = cities.count == 0 ? self.getSavedCities(): cities
        return LocalStorage.removeCityFromArray(city: city, cities: &cities)
    }
    
    static func removeCityFromArray(city: DataTypes.FavedCityData, cities: inout [DataTypes.FavedCityData]) -> [DataTypes.FavedCityData] {
        if(cities.contains(where: {$0.name == city.name})){
            cities.remove(at: cities.index(where: {$0.name == city.name})!)
        }
        return cities
    }
    
    func saveListOfCities(array: [DataTypes.FavedCityData], onCitiesSaved complete: @escaping (()) -> Void){
        DispatchQueue.global().async {
            self.persistString(withKey: StorageKeys.favedCitiesKey, withValue: DataTypes.FavedCityData.jsonArray(array: array))
            DispatchQueue.main.async {
                complete()
            }
        }
        
    }
    
    static func clearCities(onCitiesCleared complete: @escaping (()) -> Void){
        DispatchQueue.global().async {
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            UserDefaults.standard.synchronize()
            DispatchQueue.main.async {
                complete()
            }
        }
        
    }
    
    
    /*
     Units operations
    */
    
    func getCurrentUnitSystem() -> String {
        return self.getString(withKey: StorageKeys.unit) ?? ""
    }
    
    
    func setCurrentUnitSystem(unit : String) {
        self.persistString(withKey: StorageKeys.unit, withValue: unit)
    }
    
    
    
    /*
     Internal operations using UserDefaults
    */
    
    
    
    func getString(withKey key: String) -> String? {
        return UserDefaults.standard.value(forKey: key) as? String
    }
    
    func delete(withKey key: String){
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func persistString(withKey key: String, withValue string: String?){
        self.delete(withKey: key)
        guard let value = string else {
            return
        }
        UserDefaults.standard.setValue(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    
    

}
