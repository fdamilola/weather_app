//
//  Config.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 30/06/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation

struct Config {
    
    static func loadAppId() -> String {
        return Config.loadDictionary(plistname: "Config")["API_KEY"] as! String
    }
    
    
    private static func loadDictionary(plistname name:String) -> [String: AnyObject] {
        if let path = Bundle.main.path(forResource: name, ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            return dict
        }
        return [:]
    }
}




