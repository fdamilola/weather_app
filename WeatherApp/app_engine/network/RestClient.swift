//
//  RestClient.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 30/06/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation
import MapKit

class RestClient: NSObject {

    typealias Error = String
    typealias ResponseCallback = ((DataTypes.JsonObject?, Error?) -> ())
    
    var apiTask: URLSessionTask? = nil
    
    static func instantiate() -> RestClient {
        return RestClient()
    }
    
    //Get City Weather
    func getCityWeather(geocoordinate cood: CLLocationCoordinate2D, callback: @escaping ResponseCallback){
        guard let url = URL(string: ApiEndPoints.getWeather(geocoordinate: cood, unit: LocalStorage.initialize().getCurrentUnitSystem())) else {
            Logger.logMessage("Error: URL couldn't be created")
            return
        }
        
        Logger.logMessage("Calling url -> \(url.absoluteString)")
        self.callApi(url: url, callback: callback)
    }
    
    //Get City Forecast
    func getCityForecast(geocoordinate cood: CLLocationCoordinate2D, callback: @escaping ResponseCallback){
        guard let url = URL(string: ApiEndPoints.getForecast(geocoordinate: cood, unit: LocalStorage.initialize().getCurrentUnitSystem())) else {
            Logger.logMessage("Error: URL couldn't be created")
            return
        }
        Logger.logMessage("Calling url -> \(url.absoluteString)")
        self.callApi(url: url, callback: callback)
    }
    
    
    
    
    //Internal api call method
    private func callApi(url: URL,  callback: @escaping ResponseCallback){
        let request = URLRequest(url: url)
        self.apiTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                callback(nil, (error?.localizedDescription)!)
                return
            }
            
            guard let dataResponse = data else {
                callback(nil, ErrorTypes.networkError)
                return
            }
            
            do {
                guard let json = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? DataTypes.JsonObject else {
                    callback(nil, ErrorTypes.invalidJson)
                    return
                }
                callback(json, nil)
                return
            } catch {
                
            }
            
        }
        
        apiTask?.resume()
    }
    
    
    public func cancelTask(){
        self.apiTask?.cancel()
    }
    

}
