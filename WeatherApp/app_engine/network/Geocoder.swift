//
//  Geocoder.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 02/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation
import MapKit

class Geocoder {
    
    typealias CityName = String
    typealias CountryName = String
    typealias Error = String
    typealias LocationCallback = ((CityName?, CountryName?, Error?) -> ())
    
    static let shared = Geocoder()
    
    func getCityDetails(coordinate: CLLocationCoordinate2D, callback: @escaping LocationCallback){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                callback(nil, nil, ErrorTypes.networkError)
                return
            }
            callback(addressDict["City"] as? Geocoder.CityName, addressDict["Country"] as? Geocoder.CountryName , nil)
            
        })
    }
}
