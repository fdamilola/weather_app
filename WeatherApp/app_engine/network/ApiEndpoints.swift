//
//  Endpoints.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 30/06/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation
import MapKit

struct ApiEndPoints {
    
    //Api should preferably be in https://
    static let baseUrl = "http://api.openweathermap.org/data/2.5/"
    
    static func getWeather(geocoordinate cood: CLLocationCoordinate2D, unit: String) -> String{
        return "\(ApiEndPoints.baseUrl)weather?lat=\(cood.latitude)&lon=\(cood.longitude)&appid=\(Config.loadAppId())&units=\(unit)"
    }
    
    static func getForecast(geocoordinate cood: CLLocationCoordinate2D, unit: String) -> String {
        return "\(ApiEndPoints.baseUrl)forecast?lat=\(cood.latitude)&lon=\(cood.longitude)&appid=\(Config.loadAppId())&units=\(unit)"
    }
}
