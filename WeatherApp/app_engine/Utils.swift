//
//  Utils.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 02/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation
class Logger {
    //Toggle on and off to disable error logging
    static let on: Bool = false
    static func logMessage(_ message: Any){
        if Logger.on {
            debugPrint(message)
        } else{
            debugPrint("Logger is OFF")
        }
    }
}

struct Serializer {
    typealias JsonObject = [String: AnyObject]
    typealias JsonArray = [AnyObject]
    
    static func toJsonString(json: JsonObject) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch let error {
            Logger.logMessage("error converting to json: \(error)")
            return nil
        }
    }
    
    static func dataToJsonObject(data: Data?) -> JsonObject? {
        do {
            guard let dataD = data, let json = try JSONSerialization.jsonObject(with: dataD, options: []) as? JsonObject else {
                return nil
            }
            return json
        } catch {
            Logger.logMessage("Error in convertion to JsonObject")
            return nil
        }
    }
    
    static func toJsonObject(jsonString: String) -> JsonObject {
        return Serializer.dataToJsonObject(data: jsonString.data(using: String.Encoding.utf8)) ?? [:]
    }
    
    //JsonArray
    static func toJsonArrayString(json: JsonArray) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch let error {
            Logger.logMessage("error converting to json: \(error)")
            return nil
        }
    }
    
    static func dataToJsonArray(data: Data?) -> JsonArray? {
        do {
            guard let dataD = data, let jsonArray = try JSONSerialization.jsonObject(with: dataD, options: []) as? JsonArray else {
                return nil
            }
            return jsonArray
        } catch {
            Logger.logMessage("Error in convertion to JsonObject")
            return nil
        }
    }
    
    static func toJsonArray(jsonArrayString: String) -> JsonArray {
        return Serializer.dataToJsonArray(data: jsonArrayString.data(using: String.Encoding.utf8)) ?? []
    }
}
