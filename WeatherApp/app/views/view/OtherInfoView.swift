//
//  OtherInfoView.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 01/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

class OtherInfoView: UIView {
    static let name = "OtherInfoView"
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func setUpView(value: String, description: String) {
        self.valueLabel.text = value
        self.descriptionLabel.text = description
    }

    
    
}
