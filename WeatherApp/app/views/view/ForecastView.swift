//
//  ForecastView.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 01/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

class ForecastView: UIView {
    static let name = "ForecastView"
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    
    var date: Double = 0
    
    func setUp(forecast: DataTypes.Forecast) -> ForecastView {
        self.date = forecast.date
        self.dayLabel.text = forecast.dayShort
        self.temperatureLabel.text = "\(forecast.temperature)\(Units.getCurrentTempUnit())"
        self.weatherDescriptionLabel.text = forecast.mainWeatherDescription
        return self
    }
    
}
