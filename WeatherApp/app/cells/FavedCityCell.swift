//
//  FavedCityCell.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 30/06/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

class FavedCityCell: UITableViewCell {
    static let reuseIdentifier = "favedCityCell"
    @IBOutlet weak var cityNameLabel: UILabel!
    
    func setUpCell(favCity: DataTypes.FavedCityData) -> FavedCityCell {
        cityNameLabel.text = favCity.name
        return self
    }
}
