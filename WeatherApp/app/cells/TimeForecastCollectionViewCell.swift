//
//  TimeForecastCollectionViewCell.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 03/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

class TimeForecastCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "timeForecastCell"
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    func setUp(forecast: DataTypes.Forecast) -> TimeForecastCollectionViewCell {
        Logger.logMessage("Date -> \(forecast.date)")
        self.timeLabel.text = Date(timeIntervalSince1970: forecast.date).toTimeOfDay()
        self.tempLabel.text = "\(forecast.temperature)\(Units.getCurrentTempUnit())"
        self.descLabel.text = forecast.mainWeatherDescription
        return self
    }
    
}
