//
//  Constants.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 01/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import Foundation

enum SegueKeys {
    static let segueCityWeather = "segueCityWeather"
    static let segueAddCity = "segueAddCity"
    static let segueHelp = "segueHelp"
    static let segueDateForecast = "segueDateForecast"
}
