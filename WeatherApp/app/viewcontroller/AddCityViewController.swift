//
//  AddCityViewController.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 30/06/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit
import MapKit

class AddCityViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var addCityButton: UIButton!
    
    var locationManager = CLLocationManager()
    let annotation = MKPointAnnotation()
    
    var runCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Locate Me", style: .plain, target: self, action: #selector(locateMe(_:)))
        self.locationManager.delegate = self
        self.setUpMapTapping()
        self.checkAuthorizationStatus(status: CLLocationManager.authorizationStatus(), manager: self.locationManager)
    }
    
    func locateMe(_ barItem: UIBarButtonItem) {
        self.centerMapOnLocation(location: self.mapView.userLocation.coordinate)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Reset count anytime the view controller appears
        self.runCount = 0
    }
    

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == SegueKeys.segueAddCity){
            (segue.destination as! SaveCityViewController).annotation = self.annotation
        }
    }
    
    
    @IBAction func actionAddCity(_ sender: UIButton) {
        self.title = nil
        self.performSegue(withIdentifier: SegueKeys.segueAddCity, sender: self)
    }

}

extension AddCityViewController{
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let region = MKCoordinateRegionMakeWithDistance(location, 500 * 2, 500 * 2)
        self.mapView.setRegion(region, animated: true)
        self.addAnnotation(coordinate: location)
    }
    
    func checkAuthorizationStatus(status: CLAuthorizationStatus, manager: CLLocationManager){
        if(status == .authorizedWhenInUse){
            self.mapView.showsUserLocation = true
        }else{
            manager.requestWhenInUseAuthorization()
        }
    }
    
}

extension AddCityViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.checkAuthorizationStatus(status: status, manager: manager)
    }

}

extension AddCityViewController: UIGestureRecognizerDelegate {
    func setUpMapTapping(){
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(mapTapped(sender:)))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
        mapView.delegate = self
    }
    
    func mapTapped(sender: UITapGestureRecognizer) {
        let location = sender.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
        self.centerMapOnLocation(location: coordinate)
        self.addAnnotation(coordinate: coordinate)
    }
    
    func addAnnotation(coordinate: CLLocationCoordinate2D){
        mapView.removeAnnotation(self.annotation)
        self.annotation.coordinate = coordinate
        mapView.addAnnotation(self.annotation)
        self.loadingView.isHidden = true
        self.addCityButton.isEnabled = true
    }
}

extension AddCityViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if(self.runCount == 0){
            self.centerMapOnLocation(location: userLocation.coordinate)
            self.runCount += 1
        }
        
    }
}




