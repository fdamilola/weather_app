//
//  HelpViewController.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 03/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Help"
        // Do any additional setup after loading the view.
        self.webView.loadRequest(URLRequest(url: URL(string: "https://medium.com/@fdamilola/weather-app-user-manual-76bc1f7de291")!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
