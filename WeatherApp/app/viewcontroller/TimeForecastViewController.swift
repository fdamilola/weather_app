//
//  TimeForecastViewController.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 03/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

class TimeForecastViewController: UIViewController {

    
    @IBOutlet weak var dayOfWeekLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var dayOfWeek: String!
    var cityName: String!
    var countryName: String!
    
    var timeForecasts: [DataTypes.Forecast] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setTitleAndSubtitle(title: cityName, subtitle: countryName)
        self.setUpViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.collectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setUpViews(){
        self.collectionView.dataSource = self
        self.dayOfWeekLabel.text = "Timed weather forecast for \(self.dayOfWeek!)"
    }

}


extension TimeForecastViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.timeForecasts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return (collectionView.dequeueReusableCell(withReuseIdentifier: TimeForecastCollectionViewCell.identifier, for: indexPath) as! TimeForecastCollectionViewCell).setUp(forecast: self.timeForecasts[indexPath.row])
    }
}
