//
//  SaveCityViewController.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 02/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit
import MapKit

class SaveCityViewController: UIViewController {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var shortDescription: UITextField!
    
    @IBOutlet weak var coordinateLabel: UILabel!
    @IBOutlet weak var loadingLabel: UILabel!
    
    var annotation: MKAnnotation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Save City"
        self.setUpView(annotation: annotation)
        self.hideKeyboardWhenTappedAround()
        self.setUpTextFieldDelegates()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUpView(annotation: MKAnnotation) {
        self.coordinateLabel.text = "Latitude: \(annotation.coordinate.latitude.to2Sf()) || Longitude: \(annotation.coordinate.longitude.to2Sf())"
        self.loadingIndicator.startAnimating()
        self.disableTextFields(status: false)
        Geocoder.shared.getCityDetails(coordinate: annotation.coordinate) { (cityName, countryName, error) in
            self.loadingIndicator.stopAnimating()
            if(error == nil){
                self.cityTextField.text = cityName
                self.countryTextField.text = countryName
                self.shortDescription.becomeFirstResponder()
            }else{
                self.cityTextField.becomeFirstResponder()
            }
            self.disableTextFields(status: true)
        }
    }
    
    func disableTextFields(status: Bool){
        self.cityTextField.isEnabled = status
        self.countryTextField.isEnabled = status
        self.loadingLabel.isHidden = status
    }
    
    func setUpTextFieldDelegates(){
        self.cityTextField.addDoneToKeyboard()
        self.countryTextField.addDoneToKeyboard()
        self.shortDescription.addDoneToKeyboard()
    }

    func saveCity(){
        guard !(self.cityTextField.text?.isEmpty)! else {
            self.createAlertDialog("Form Error!", message: "City name cannot be empty!")
            return
        }
        
        guard !(self.countryTextField.text?.isEmpty)! else {
            self.createAlertDialog("Form Error!", message: "Country name cannot be empty!")
            return
        }
        
        var favCity = DataTypes.FavedCityData()
        favCity.cityDescription = self.shortDescription.getCleanString()
        favCity.cityName = self.cityTextField.getCleanString()
        favCity.countryName = self.countryTextField.getCleanString()
        favCity.latitude = self.annotation.coordinate.latitude
        favCity.longitude = self.annotation.coordinate.longitude
        LocalStorage.initialize().saveCity(city: favCity, sprotocol: self)
    }
    
    @IBAction func actionSaveCity(_ sender: UIButton) {
        let okAction = UIAlertAction(title: "YES", style: .default) { (_) in
            self.saveCity()
        }
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel)
        
        self.createAlertDialog("Save?", message: "Save '\(self.cityTextField.text!)' to your bookmarked cities?", ltrActions: [cancelAction, okAction])
    }

}

//extension SaveCityViewController: UITextFieldDelegate {
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        self.dismissKeyboard()
//        return true
//    }
//}

extension SaveCityViewController: CityProtocol {
    func onSaved(savedCity city: DataTypes.FavedCityData) {
        
        let addAnotherAction = UIAlertAction(title: "YES", style: .default) { (_) in
            self.navigationController?.popViewController(animated: true)
        }
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel) { (_) in
            self.loadStoryboard(storyboard_name: "Main")
        }
        
        self.createAlertDialog("'\(city.cityName)' successfully saved!", message: "Do you want to add another city?", ltrActions: [cancelAction, addAnotherAction])
    }
}
