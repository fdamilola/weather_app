//
//  ViewController.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 30/06/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

class BookmarkedCitiesViewController: UIViewController {
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyViewLabel: UILabel!
    @IBOutlet weak var addCityButton: UIButton!
    
    @IBOutlet weak var cityTableView: UITableView!
    
    lazy var cityArray: [DataTypes.FavedCityData] = []
    lazy var selectedCity: DataTypes.FavedCityData? = nil
    
    var searchController: UISearchController?  = UISearchController(searchResultsController: nil)
    lazy var filteredCities: [DataTypes.FavedCityData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.definesPresentationContext = true
        self.setUpViews()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.title = "Saved Cities"
        self.setUpSearchBar()
        self.reload()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchController?.isActive = false
    }
    
    @IBAction func addCityAction(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 1
    }
    
    
    private func setUpViews(){
        cityTableView.delegate = self
        cityTableView.dataSource = self
    }
    
    func reload(){
        DispatchQueue.global().async {
            self.cityArray = LocalStorage.initialize().getReversedSavedCities()
            DispatchQueue.main.async {
                self.cityTableView.isHidden = self.toggleState(array: self.cityArray)
                self.emptyViewLabel.isHidden = !self.toggleState(array: self.cityArray)
                self.addCityButton.isHidden = !self.toggleState(array: self.cityArray)
                self.cityTableView.reloadData()
            }
        }
    }
    
    private func toggleState(array: [DataTypes.FavedCityData]) -> Bool {
        return array.count > 0 ? false : true
    }
    
    
}

extension BookmarkedCitiesViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == SegueKeys.segueCityWeather){
            self.title = nil
            (segue.destination as! CityWeatherViewController).city = self.selectedCity!
        }
    }
}

extension BookmarkedCitiesViewController: UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate{

    func setUpSearchBar(){
        self.searchController?.searchResultsUpdater = self
        self.searchController?.delegate = self;
        self.searchController?.searchBar.delegate = self;
        self.searchController?.dimsBackgroundDuringPresentation = false
        self.searchController?.hidesNavigationBarDuringPresentation = false;
        self.searchController?.definesPresentationContext = true;
        self.searchController?.searchBar.searchBarStyle = UISearchBarStyle.minimal;
        self.searchController?.searchBar.placeholder = "Search for city"
        
        self.navigationController?.navigationBar.topItem?.titleView = self.searchController?.searchBar;
        self.navigationController?.navigationBar.topItem?.titleView?.tintColor = UIColor.blue
        
    }

    
    func didPresentSearchController(_ searchController: UISearchController) {
        self.searchController?.searchBar.showsCancelButton = true
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
    func filterContentForSearchText(searchText: String) {
        self.filteredCities = self.cityArray.filter({ $0.name.lowercased().contains(searchText.lowercased())})
        self.cityTableView.reloadData()
        
    }
}

extension BookmarkedCitiesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        self.selectedCity = getAppropriateArray()[indexPath.row]
        self.performSegue(withIdentifier: SegueKeys.segueCityWeather, sender: nil)
    }
}

extension BookmarkedCitiesViewController: UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getAppropriateArray().count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return (tableView.dequeueReusableCell(withIdentifier: FavedCityCell.reuseIdentifier, for: indexPath) as! FavedCityCell).setUpCell(favCity: getAppropriateArray()[indexPath.row])
    }
    
    func getAppropriateArray() -> [DataTypes.FavedCityData] {
        return (self.searchController?.isActive)! && self.searchController?.searchBar.text != ""
            ? self.filteredCities:self.cityArray
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let cityToRemove = self.getAppropriateArray()[indexPath.row]
            let deleteAction = UIAlertAction(title: "YES", style: .destructive, handler: { (_) in
                if (self.searchController?.isActive)! && self.searchController?.searchBar.text != "" {
                    self.filteredCities.remove(at: indexPath.row)
                }
                LocalStorage.initialize().saveListOfCities(array: LocalStorage.removeCityFromArray(city: cityToRemove, cities: &self.cityArray), onCitiesSaved: { (_) in
                    self.reload()
                })
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            self.createAlertDialog("Delete?", message: "Are you sure you want to delete \(cityToRemove.name)?", ltrActions: [cancelAction, deleteAction])
        }
    }
    
}

