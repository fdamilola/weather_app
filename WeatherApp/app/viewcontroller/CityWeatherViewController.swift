//
//  CityWeatherViewController.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 01/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

class CityWeatherViewController: UIViewController {

    var city: DataTypes.FavedCityData!
    
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var loadingForecastIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var otherInfoStackView: UIStackView!
    @IBOutlet weak var forecastStackView: UIStackView!
    
    let restClient: RestClient? = RestClient.instantiate()
    
    lazy var fullForecastArray: [DataTypes.Forecast] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setTitleAndSubtitle(title: city.cityName, subtitle: city.countryName)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Details", style: .plain, target: self, action: #selector(showDetails(_:)))
        self.loadWeatherInfo(city: city)
        self.loadCityForecast(city: city)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        guard self.restClient != nil else {
            Logger.logMessage("RestClient instant is nil")
            return
        }
        self.restClient?.cancelTask()
    }
    
    func showDetails(_ barItem: UIBarButtonItem) {
        //TODO :- Add option to edit/update city description. It's currently a passive feature
        self.createAlertDialog(self.city.name, message: self.city.cityDescription.isEmpty ? "No description has been added yet!":self.city.cityDescription)
    }
    
    func loadWeatherInfo(city: DataTypes.FavedCityData){
        self.loadingIndicatorView.startAnimating()
        self.restClient?.getCityWeather(geocoordinate: city.geoCoordinate) { (jsonObject, error) in
            if error == nil {
                let weather = DataTypes.Weather.fromJsonObject(data: jsonObject!)
                Logger.logMessage(weather.weatherDescription)
                DispatchQueue.main.async {
                    self.loadingIndicatorView.stopAnimating()
                    self.setUpViews(weather: weather)
                }
            }else{
                Logger.logMessage(error!)
            }
        }
    }
    
    func loadCityForecast(city: DataTypes.FavedCityData){
        self.loadingForecastIndicatorView.startAnimating()
        self.restClient?.getCityForecast(geocoordinate: city.geoCoordinate) { (jsonObject, error) in
            if error == nil {
                Logger.logMessage(jsonObject!)
                let forecastArray = DataTypes.Forecast.daysFromJsonArray(array: jsonObject!["list"] as! [DataTypes.JsonObject])
                self.fullForecastArray = DataTypes.Forecast.fullForecastFromJsonArray(array: jsonObject!["list"] as! [DataTypes.JsonObject])
                DispatchQueue.main.async {
                    self.loadingForecastIndicatorView.stopAnimating()
                    self.setUpForecastViews(array: forecastArray)
                }
            }else{
                Logger.logMessage(error!)
            }
        }
    }
    
    func setUpViews(weather: DataTypes.Weather){
        
        self.dateLabel.text = Date(timeIntervalSince1970: weather.date).toHumanReadableFormat()
        self.weatherImageView.image = IconLoader.loadIcon(iconId: weather.icon)
        self.weatherDescriptionLabel.text = weather.weatherDescriptionTitleCased
        self.temperatureLabel.text = "\(weather.temperature)\(Units.getCurrentTempUnit())"
        
        //Created different variables for easy readability. Can be optimized further but time constraints
        let humidityView = createOtherInfoView()
        let rainChanceView = createOtherInfoView()
        let windSpeedView = createOtherInfoView()
        
        humidityView.setUpView(value: "\(weather.humidity)", description: "Humidity (%)")
        rainChanceView.setUpView(value: "\(weather.rainChances)", description: "Rain (%)")
        windSpeedView.setUpView(value: "\(weather.windSpeed)", description: "Wind (\(Units.getCurrentWindUnit()))")
        
        self.otherInfoStackView.addArrangedSubview(humidityView)
        self.otherInfoStackView.addArrangedSubview(rainChanceView)
        self.otherInfoStackView.addArrangedSubview(windSpeedView)
        
    }
    
    func setUpForecastViews(array: [DataTypes.Forecast]){
        for forecast in array {
            let tap = UITapGestureRecognizer(target: self, action: #selector((forecastTapped(_:))))
            let forecastView = createForecastView()
            forecastView.isUserInteractionEnabled = true
            forecastView.addGestureRecognizer(tap)
            self.forecastStackView.addArrangedSubview(forecastView.setUp(forecast: forecast))
        }
    }
    
    
    lazy var arrayOfForecasts: [DataTypes.Forecast] = []
    lazy var dayOfWeek: String = ""
    func forecastTapped(_ tap: UITapGestureRecognizer) {
        let forecastView = tap.view as! ForecastView
        let date = Date(timeIntervalSince1970: forecastView.date)
        self.dayOfWeek = date.toDayOfWeek()
        DispatchQueue.global().async {
            self.arrayOfForecasts = DataTypes.Forecast.forcastForDate(today: date, array: self.fullForecastArray)
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: SegueKeys.segueDateForecast, sender: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.title = nil
        if(segue.identifier == SegueKeys.segueDateForecast){
            let dest = segue.destination as! TimeForecastViewController
            dest.cityName = self.city.cityName
            dest.countryName = self.city.countryName
            dest.dayOfWeek = self.dayOfWeek
            dest.timeForecasts = self.arrayOfForecasts
        }
    }
    
}

extension CityWeatherViewController{
    func createForecastView() -> ForecastView {
        return self.loadView(viewName: ForecastView.name) as! ForecastView
    }
    
    func createOtherInfoView() -> OtherInfoView {
        return self.loadView(viewName: OtherInfoView.name) as! OtherInfoView
    }
}
