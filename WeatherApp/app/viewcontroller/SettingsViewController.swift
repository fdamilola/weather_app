//
//  SettingsViewController.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 30/06/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var unitSegmentControl: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Help", style: .plain, target: self, action: #selector(showHelp(_:)))
        self.unitSegmentControl.selectedSegmentIndex = self.getSavedUnitIndex()
    }
    
    func showHelp(_ barItem: UIBarButtonItem) {
        self.performSegue(withIdentifier: SegueKeys.segueHelp, sender: self)
    }
    
    @IBAction func actionUnitChanged(_ sender: UISegmentedControl) {
        LocalStorage.initialize().setCurrentUnitSystem(unit: sender.selectedSegmentIndex == 1 ? Units.metric: sender.selectedSegmentIndex == 2 ? Units.imperial: Units.standard)
    }
    
    func getSavedUnitIndex() -> Int {
        let unitString = LocalStorage.initialize().getCurrentUnitSystem()
        return unitString == Units.metric ? 1 : unitString == Units.imperial ? 2 : 0
    }

    @IBAction func actionClearCities(_ sender: UIButton) {
        self.clearCities()
    }
    
    func clearCities() {
        let yesAction = UIAlertAction(title: "YES!", style: .destructive) { (_) in
            LocalStorage.clearCities {
                self.createAlertDialog("Success!", message: "Saved cities successfully cleared")
            }
        }
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: nil)
        self.createAlertDialog("Clear Cities?", message: "Are you sure you want to clear saved cities?", ltrActions: [cancelAction, yesAction])
        
    }

}
