//
//  GenericExtensions.swift
//  WeatherApp
//
//  Created by Akapo Damilola Francis on 01/07/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import UIKit


/*
 Views && Related DS
 */

extension UINavigationItem {
    func setTitleAndSubtitle(title:String, subtitle:String) {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: -2, width: 0, height: 0))
        
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.gray
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        let subtitleLabel = UILabel(frame: CGRect(x: 0, y: 18, width: 0, height: 0))
        subtitleLabel.backgroundColor = UIColor.clear
        subtitleLabel.textColor = UIColor.black
        subtitleLabel.font = UIFont.systemFont(ofSize: 12)
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), height: 30))
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)
        
        let widthDiff = subtitleLabel.frame.size.width - titleLabel.frame.size.width
        
        if widthDiff < 0 {
            let newX = widthDiff / 2
            subtitleLabel.frame.origin.x = abs(newX)
        } else {
            let newX = widthDiff / 2
            titleLabel.frame.origin.x = newX
        }
        
        self.titleView = titleView
    }
}

extension UIViewController {
    
    func setTitleAndSubtitle(title:String, subtitle:String){
        self.navigationItem.setTitleAndSubtitle(title: title, subtitle: subtitle)
    }
    
    public func loadView(viewName name:String!) -> UIView {
        return Bundle.main.loadNibNamed(name, owner: self, options: nil)?[0] as! UIView
    }
    
    
    func createAlertDialog(_ title: String! = "Oops!", message: String! = "Network error!.", ltrActions: [UIAlertAction]! = []) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        
        if(ltrActions.count == 0){
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil);
            alertController.addAction(defaultAction);
        }else{
            for x in ltrActions{
                alertController.addAction(x as UIAlertAction);
            }
        }
        
        self.present(alertController, animated: true, completion: nil);
    }
    
    func loadStoryboard(storyboard_name name:String!){
        OperationQueue.main.addOperation {
            [weak self] in
            let storyboard = UIStoryboard(name: name, bundle: nil)
            let home = storyboard.instantiateInitialViewController()
            self?.present(home!, animated: true, completion: nil);
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false;
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
}

extension UITextField {
    func getCleanString() -> String {
        return self.text == nil ? "":self.text!
    }
    
    func addDoneToKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem:
            .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem:
            .done, target: self, action: #selector(UIViewController.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.inputAccessoryView = keyboardToolbar
    }
    
    func dismissKeyboard() {
        self.superview?.endEditing(true)
    }
}

/*
 DSs
 */

extension String {
    var first: String {
        return String(characters.prefix(1))
    }
    var last: String {
        return String(characters.suffix(1))
    }
    var titleCase: String {
        return first.uppercased() + String(characters.dropFirst())
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
}

extension Double {
    func to2Sf() -> String {
        return String(format: "%.2f", self)
    }
}

extension Date {
    func toHumanReadableFormat() -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "EEEE dd, MMMM, yyyy"
        return dateFormatter.string(from: self);
    }
    
    func toDayOfWeek() -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self);
    }
    
    func toTimeOfDay() -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.none
        dateFormatter.timeStyle = DateFormatter.Style.medium
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "HH:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: self);
    }
    
    func daysBetween(date: Date) -> Int {
        return Date.daysBetween(start: self, end: date)
    }
    
    static func daysBetween(start: Date, end: Date) -> Int {
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        
        let a = calendar.dateComponents([.day], from: date1, to: date2)
        return a.value(for: .day)!
    }
}


