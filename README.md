# README #

## Weather App iOS repository ##

> The App allows the user bookmark cities of interest and check up their general weather condition when needed.

### URL To Source SourceCode

[Source Code](https://bitbucket.org/Helios66/weather_app)


### Manual

[Weather App Manual](https://medium.com/@fdamilola/weather-app-user-manual-76bc1f7de291)

### Build Status ###

> NeverCode CI

[![Nevercode build status](https://app.nevercode.io/api/projects/62925c4c-8dc5-4e8b-b2be-d8c7a8328350/workflows/0315c90e-2071-493f-bbd4-6f9adea2e792/status_badge.svg?branch=master)](https://app.nevercode.io/#/project/62925c4c-8dc5-4e8b-b2be-d8c7a8328350/workflow/0315c90e-2071-493f-bbd4-6f9adea2e792/latestBuild?branch=master)

### How do I get set up?

* Install the latest version of xCode with support for Swift 3.1 or higher.

* Clone the repository `git clone git@bitbucket.org:Helios66/weather_app.git`

* Build and install on device of choice with iOS 9.0 and higher.

### Notes ###
- Used UserDefaults for storage because of assumptive stance of data to be saved being small. SQLite or ORM would be a preferable option for a much more scalable one.


### Contributors ###

* Akapo Damilola [fdamilola@gmail.com](mailto:fdamilola@gmail.com)
