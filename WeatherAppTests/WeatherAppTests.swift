//
//  WeatherAppTests.swift
//  WeatherAppTests
//
//  Created by Akapo Damilola Francis on 30/06/2017.
//  Copyright © 2017 Backbase. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeatherAppTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of 
        //each test method in the class.
        
        LocalStorage.clearCities {
            Logger.logMessage("Clearing Cities")
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSaveAndGet(){
        LocalStorage.initialize().persistString(withKey: "randomKey", withValue: "randomValue")
        XCTAssertTrue(LocalStorage.initialize().getString(withKey: "randomKey") == "randomValue", "Saved randomKey")
    }
    
    func testDelete(){
        LocalStorage.initialize().persistString(withKey: "randomKey", withValue: "randomValue")
        LocalStorage.initialize().delete(withKey: "randomKey")
        XCTAssertFalse(LocalStorage.initialize().getString(withKey: "randomKey") == "randomValue", "Deleted randomKey")
    }
    
    func testLocalStorageSave(){
        var city = DataTypes.FavedCityData()
        city.cityName = "Ibadan"
        city.countryName = "Nigeria"
        city.longitude = 3.71
        city.latitude = 4.78
        city.cityDescription = "Some description"
        LocalStorage.initialize().saveCity(city: city)
        XCTAssertTrue(LocalStorage.initialize().getSavedCities().contains(where: {$0.name == city.name}), "City saved successfully")
    }
    
    
    func testLocalStorageClear(){
        LocalStorage.clearCities { (_) in
            XCTAssertTrue(LocalStorage.initialize().getSavedCities().count == 0,
                          "Cities successfully cleared")
        }
    }
    
    func testLocalStorageMultipleCities(){
        var city = DataTypes.FavedCityData()
        city.cityName = "Ibadan"
        city.countryName = "Nigeria"
        city.longitude = 3.71
        city.latitude = 4.78
        
        var city2 = DataTypes.FavedCityData()
        city2.cityName = "Amsterdam"
        city2.countryName = "Netherlands"
        city2.longitude = 70.91
        city2.latitude = 29.80
        
        var city3 = DataTypes.FavedCityData()
        city3.cityName = "Sovengrande"
        city3.countryName = "Cheese"
        city3.longitude = 2.00
        city3.latitude = 41.78
        
        LocalStorage.initialize().saveCity(city: city)
        LocalStorage.initialize().saveCity(city: city2)
        LocalStorage.initialize().saveCity(city: city3)
        
        XCTAssertTrue(LocalStorage.initialize().getSavedCities().contains(where:
            {$0.name == city.name}), "City1 saved successfully")
        XCTAssertTrue(LocalStorage.initialize().getSavedCities().contains(where:
            {$0.name == city2.name}), "City2 saved successfully")
        XCTAssertTrue(LocalStorage.initialize().getSavedCities().contains(where:
            {$0.name == city3.name}), "City3 saved successfully")
        
    }
    
    func testSingleCityDelete(){
        
        var city = DataTypes.FavedCityData()
        city.cityName = "Ibadan"
        city.countryName = "Nigeria"
        city.longitude = 3.71
        city.latitude = 4.78
        
        var city2 = DataTypes.FavedCityData()
        city2.cityName = "Amsterdam"
        city2.countryName = "Netherlands"
        city2.longitude = 70.91
        city2.latitude = 29.80
        
        var city3 = DataTypes.FavedCityData()
        city3.cityName = "Sovengrande"
        city3.countryName = "Cheese"
        city3.longitude = 2.00
        city3.latitude = 41.78
        
        LocalStorage.initialize().saveCity(city: city)
        LocalStorage.initialize().saveCity(city: city2)
        LocalStorage.initialize().saveCity(city: city3)
        
        XCTAssertFalse(LocalStorage.initialize().removeCity(city:
            city2).contains(where: {$0.name == city2.name}), "City2 deleted")
    }

    
    func testLocalStorageSaveTime(){
        self.measure {
            self.testLocalStorageSave()
        }
    }
    
    func testLocalStorageDeleteTime() {
        self.measure {
            LocalStorage.clearCities {
                
            }
        }
    }
    
}
